<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlightBook extends Model
{
    use HasFactory;

    protected $fillable = [
        'airline_id',
        'flight_type',
        'from_date',
        'from_time',
        'return_date',
        'return_time',
        'from_city_name',
        'to_city_name',
        'flight_class',
        'total_adults',
        'total_children',
//        'first_name',
//        'last_name',
    ];
}
