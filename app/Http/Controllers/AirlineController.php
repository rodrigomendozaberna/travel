<?php

namespace App\Http\Controllers;

use App\Models\Airline;
use App\Http\Requests\StoreAirlineRequest;
use App\Http\Requests\UpdateAirlineRequest;
use App\Models\Flight;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class AirlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $airline = Airline::all();
        return View::make("pages.home")->with("airline", $airline);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreAirlineRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAirlineRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Airline $airline
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $flight = Flight::find($id);

        return View::make("pages.purchase")->with("flight", $flight);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Airline $airline
     * @return \Illuminate\Http\Response
     */
    public function edit(Airline $airline)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateAirlineRequest $request
     * @param \App\Models\Airline $airline
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAirlineRequest $request, Airline $airline)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Airline $airline
     * @return \Illuminate\Http\Response
     */
    public function destroy(Airline $airline)
    {
        //
    }

    public function flight(Request $request)
    {
        $flights = DB::table('flights')
            ->where('from_city_name', '=', $request->fromPort)
            ->where('to_city_name', '=', $request->toPort)
            ->get();

        //return $flights;
        return View::make("pages.flights")->with("flights", $flights);
    }

    public function confirmation(Request $request)
    {
        $purchase = $request->flight_reserve;
        $purchase = json_decode($purchase, true);
        return View::make("pages.confirmation")->with("purchase", $purchase);
        //return $purchase;
    }
}
