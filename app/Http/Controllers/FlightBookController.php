<?php

namespace App\Http\Controllers;

use App\Models\FlightBook;
use App\Http\Requests\StoreFlightBookRequest;
use App\Http\Requests\UpdateFlightBookRequest;

class FlightBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFlightBookRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFlightBookRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FlightBook  $flightBook
     * @return \Illuminate\Http\Response
     */
    public function show(FlightBook $flightBook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FlightBook  $flightBook
     * @return \Illuminate\Http\Response
     */
    public function edit(FlightBook $flightBook)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFlightBookRequest  $request
     * @param  \App\Models\FlightBook  $flightBook
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFlightBookRequest $request, FlightBook $flightBook)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FlightBook  $flightBook
     * @return \Illuminate\Http\Response
     */
    public function destroy(FlightBook $flightBook)
    {
        //
    }
}
