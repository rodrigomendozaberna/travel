<?php

namespace App\Http\Controllers;

use App\Models\Travels;
use App\Http\Requests\StoreTravelsRequest;
use App\Http\Requests\UpdateTravelsRequest;

class TravelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTravelsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTravelsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Travels  $travels
     * @return \Illuminate\Http\Response
     */
    public function show(Travels $travels)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Travels  $travels
     * @return \Illuminate\Http\Response
     */
    public function edit(Travels $travels)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTravelsRequest  $request
     * @param  \App\Models\Travels  $travels
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTravelsRequest $request, Travels $travels)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Travels  $travels
     * @return \Illuminate\Http\Response
     */
    public function destroy(Travels $travels)
    {
        //
    }

    public function purchase(Travels $travels)
    {
        //
    }
}
