@extends('layouts.default')
@section('content')
    <div class="pt-5">
        <h3>Flights from {{$flights[0]->from_city_name}} to {{$flights[0]->to_city_name}}: </h3>

        <div class="pt-5">
            <table class="table">
                <thead>
                <tr>
                    <th>Choose</th>
                    <th>Flight #</th>
                    <th>Airline</th>
                    <th>Departs: {{$flights[0]->from_city_name}}</th>
                    <th>Arrives: {{$flights[0]->to_city_name}}</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                @foreach($flights as $key => $val)
                    <tr>
                        <td><a href="{{route("home.show", $val->id)}}">Reserve This Flight</a></td>
                        <td>{{$val->flight_code}}</td>
                        <td>{{$val->airline_id}}</td>
                        <td>{{$val->flight_time}}</td>
                        <td>{{$val->arrival_time}}</td>
                        <td>${{$val->price}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
