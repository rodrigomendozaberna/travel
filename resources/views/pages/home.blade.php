@extends('layouts.default')
@section('content')
    <div class="pt-5">
        <h1>Welcome to the Simple Travel Agency!</h1>
        <p>The is a sample site you can test with BlazeMeter! </p>
        <p>Check out our <a href="#">destination of the week! The Beach!</a></p>
    </div>

    <div>
        <form action="{{route("home.flight")}}" method="post">
            @csrf
            <div class="mb-3">
                <div class="col-lg-6">
                    <label for="">Choose your departure city:</label>
                    <select name="fromPort" class="form-select">
                        @foreach($airline as $key => $val)
                            <option value="{{$val->city_name}}">{{$val->city_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mb-3">
                <div class="col-lg-6">
                    <label for="">Choose your destination city:</label>
                    <select name="toPort" class="form-select" required>
                        <option value="">None</option>
                        @foreach($airline as $key => $val)
                            <option value="{{$val->city_name}}">{{$val->city_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mb-3">
                <div class="col-lg-4">
                    <button type="submit" class="btn btn-primary">Find Flights</button>
                </div>
            </div>
        </form>
    </div>
@stop
