@extends('layouts.default')
@section('content')
    <style>
        .btn-success {
            background-color: blue;
            width: 100%;
            border-radius: 20px;
        }
    </style>
    <div class="pt-5">
        <h1>Thank you for your purchase today!</h1>
        <table class="table">
            <thead>
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Id</td>
                <td>{{$purchase['flight_code']}}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>PendingCapture</td>
            </tr>
            <tr>
                <td>Amount</td>
                <td>${{$purchase['price']}}</td>
            </tr>
            <tr>
                <td>Payment method</td>
                <td>PayPal</td>
            </tr>
            <tr>
                <td>Date</td>
                <td>{{date("d-m-y")}}</td>
            </tr>
            </tbody>
        </table>
        <div class="pt-5">
            <a class="btn btn-success" href="{{route("home.index")}}">Aceptar</a>
        </div>
    </div>
@stop
