@extends('layouts.default')
@section('content')

    <style>
        .btn-pay {
            background-color: yellow;
            width: 250px;
            height: 50px;
            border-radius: 20px;
            border: none;
        }

        .img-pay {
            width: 80px;
            height: auto;
        }
    </style>
    <div>
        <div class="pt-5">
            <h2>Your flight from {{$flight->from_city_name}} to {{$flight->to_city_name}} has been reserved.</h2>
            <p><strong>Airline: </strong>{{$flight->airline_id}}</p>
            <p><strong>Flight Number: </strong>{{$flight->flight_code}}</p>
            <p><strong>Price:</strong> ${{$flight->price}}</p>
            <p><strong>Arbitrary Fees and Taxes:</strong> $10</p>
            <hr>
            <p><strong>Total Cost:</strong> <em>${{$flight->price + 10}} </em></p>
            <p>Please submit the form below to purchase the flight.</p>
        </div>

        <form action="{{route("home.confirmation")}}" method="post">
            @csrf
            <input type="text" name="flight_reserve" value="{{$flight}}" hidden>
            <div class="mb-3">
                <button class="btn-pay" type="submit"><img class="img-pay" src="{{asset("pay.svg")}}" alt=""></button>
            </div>
        </form>
    </div>
@stop
