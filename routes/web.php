<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\AirlineController::class, "index"])->name("home.index");
Route::post('flight', [\App\Http\Controllers\AirlineController::class, "flight"])->name("home.flight");
Route::get('flight/{id}', [\App\Http\Controllers\AirlineController::class, "show"])->name("home.show");
Route::post('confirmation', [\App\Http\Controllers\AirlineController::class, "confirmation"])->name("home.confirmation");
