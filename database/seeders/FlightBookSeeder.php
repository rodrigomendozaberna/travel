<?php

namespace Database\Seeders;

use App\Models\FlightBook;
use Illuminate\Database\Seeder;

class FlightBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FlightBook::factory()->count(20)->create();
    }
}
