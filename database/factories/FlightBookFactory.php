<?php

namespace Database\Factories;

use App\Models\Airline;
use Illuminate\Database\Eloquent\Factories\Factory;

class FlightBookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'airline_id' => Airline::all()->random()->id,
            'flight_type' => ['one-way', 'return'][random_int(0, 1)],
            'flight_code' => $this->faker->postcode . $this->faker->countryCode,
            'from_date' => $this->faker->date(),
            'from_time' => $this->faker->time(),
            'return_date' => $this->faker->date(),
            'return_time' => $this->faker->time(),
            'from_city_name' => $this->faker->city,
            'to_city_name' => $this->faker->city,
            'total_adults' => random_int(0, 50),
            'total_children' => random_int(0, 50),
            'flight_class' => ['economy', 'business', 'first class'][rand(0, 2)],
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
        ];
    }
}
