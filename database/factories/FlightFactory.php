<?php

namespace Database\Factories;

use App\Models\Airline;
use App\Models\Flight;
use Illuminate\Database\Eloquent\Factories\Factory;

class FlightFactory extends Factory
{
    protected $model = Flight::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'flight_code' => $this->faker->postcode . $this->faker->countryCode,
            'from_date' => $this->faker->date(),
            'to_date' => $this->faker->date(),
            'flight_time' => $this->faker->time(),
            'arrival_time' => $this->faker->time(),
            'from_city_name' => Airline::all()->random()->city_name,
            'to_city_name' => Airline::all()->random()->city_name,
            'airline_id' => $this->faker->randomNumber(),
            'price' => $this->faker->numberBetween(100, 1000)
        ];
    }
}
